package id.ac.poltek_kediri.informatika.appx08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var pref : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        btnSimpanPref.setOnClickListener(this)

        pref = getSharedPreferences("setting",Context.MODE_PRIVATE)
        txKeysValues.setText(
            "Ukuran Font = ${pref.getInt("font_size",12)} \n"+
            "Isi Teks = ${pref.getString("teks","Halo Dunia")}"
        )

    }

    override fun onClick(v: View?) {
        pref = getSharedPreferences("setting",Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putInt("font_size",edUkuranFont.text.toString().toInt())
        editor.commit()
    }

}