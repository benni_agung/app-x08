package id.ac.poltek_kediri.informatika.appx08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnActSetting ->{
                startActivity(Intent(this,SettingActivity::class.java))
            }
            R.id.btnSimpan ->{
                preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(FIELD_TEXT,edText.text.toString())
                prefEditor.putInt(FIELD_FONT_SIZE,sbar.progress)
                prefEditor.commit()
                Toast.makeText(this,"Perubahan telah disimpan",Toast.LENGTH_SHORT).show()
            }
        }

    }

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"           //nama SharedPreferences, file akan bernama setting.xml
    val FIELD_FONT_SIZE = "font_size"   //key
    val FIELD_TEXT = "teks"             //key
    val DEF_FONT_SIZE = 12          //value, integer, nilai default
    val DEF_TEXT = "Hello World"    //value, string , nilai default

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            edText.setTextSize(progress.toFloat())
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //pada pemanggilan aplikasi kedua kalinya dan seterusnya,
        // kode ini artinya membuka file SharedPreferences
        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        edText.setText(preferences.getString(FIELD_TEXT,DEF_TEXT))
        edText.textSize = preferences.getInt(FIELD_FONT_SIZE, DEF_FONT_SIZE).toFloat()
        sbar.progress = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE)
        sbar.setOnSeekBarChangeListener(onSeek)
        btnSimpan.setOnClickListener(this)
        btnActSetting.setOnClickListener(this)
    }
}
